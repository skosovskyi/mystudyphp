<?php

$ary=array(); 
$i=0;
$n=40;

/*
 * @param integer $i
 * @param integer $n
 */
function UseCycles(integer $i, integer $n) : void
{

	echo "<b>" . 'Cycle do WHILE' . "</b>" . "<br>";

	do {
		$ary[$i] = $i;
		echo $i . '-' . $ary[$i] . "<br>";

	} while ($i++ < $n);

	echo "<b>" . 'Cycle WHILE'. "</b>" . "<br>";

	while ($i-- > 0) {
		$ary[$i] = $i;
		echo $i . '-' . $ary[$i] . "<br>";

	}

	echo "<b>" . 'Cycle for' . "</b>" . "<br>";

	for (; $i <= $n; $i++) {
		if ($i < 0) {
			$i = 0;
		}

		echo 'for' . '-' . $i;
		$over = $i % 2;

		if ($over) {
			$ary[$i] = 'not paired';
		}

		if (!$over) {
			$ary[$i] = 'paired';
		}

		if (!($ary[$i]!==$i) ) {
                $ary[$i]='is not number';
                }


		echo '-' . $ary[$i] . "<br>";
	}

	echo "<b>" . 'Cycle foreach' . "</b>" . "<br>";

	foreach ($ary as $key => $value) {

		echo '-' . $key . '-' . $value . "<br>";

	}
}


UseCycles($i,$n);

?>
