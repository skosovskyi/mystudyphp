<?php

$file_name='data_filePHP.csv';
$dir_file =&$file_name;

/*
 * @param string $file_name
 */

function create_file(string $file_name) : void
{
    $name = ['Jac', 'Den', 'Tom', 'Alex', 'Yong', 'Ali'];
    $in_stock = [1, 0];
    $usd = null;
    $year = null;
    $model = ['X', 'W', 'Z', 'Q', 'I', 'A', 'C', 'D'];
    $brand = ['APPLE', 'SAMSUNG', 'SONY', 'AVAYA', 'LG', 'CISCO', 'ASUS', 'ALCATEL'];
    $lines_ary = [];
    $i = rand(50, 100);
    $fp = fopen($file_name, 'wb');
    for (; $i >= 0; $i--) {
        $line_update = '';
        $line_update_fix = '';
        $lines_ary = [];
        $data_file_line =
            ['brand' => $brand[rand(0, 7)],
                'model' => $model[rand(0, 7)] . $i,
                'name' => $name[rand(0, 5)],
                'usd' => rand(1000, 5000),
                'year' => rand(2010, 2019),
                'in_stock' => $in_stock[rand(0, 1)],
            ];
        $over = $i % 2;
        if ($over) {
            krsort($data_file_line);
        }
        //shuffle($data_file_line);
        foreach ($data_file_line as $key => $value) {
            $line_update = $line_update . $key . "|" . $value . ",";

        }
        $line_update_fix = substr($line_update, 0, -1);
        array_push($lines_ary, $line_update_fix);
        fputcsv($fp, $lines_ary, ",", ' ', "\\");
    }
    fclose($fp);

}

//---------------------------------------------------------------------

/*
 * @param string $file_name
 * @return integer $count_lines
 */

function count_lines(string $dir_file)
{

    $count_lines = file("$dir_file", FILE_SKIP_EMPTY_LINES);

    return count($count_lines);
    
}

/*
 * @param string $file_name
 * @returne array $data_from_file
 */
 
function mapLinesToAry(string $dir_file)
{
    $data_from_file=[];

    $handle = fopen("$dir_file", "r");

    while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
        $item_value_update=[];
        $brand= null;
        $data_from_file_update=[];
        foreach ($data as $value){
            $item_value=explode("|", $value);
            if (trim($item_value[0])== 'brand' ) {
                $brand=trim($item_value[1]);
                if ($data_from_file[$brand] == FALSE){
                    $data_from_file_update=[$brand=>[]];
                    $data_from_file=$data_from_file+$data_from_file_update;
                }

                continue;
            }

            else {
                $item_value_update=$item_value_update + [$item_value[0] => $item_value[1]];

            }

        }

        array_push($data_from_file[$brand],$item_value_update);

    }

    fclose ( $handle );

    return $data_from_file;

}

/*
 * @param string $file_name
 */
 
function file_in_page(string $dir_file): void
{
    foreach (mapLinesToAry($dir_file) as $key=>$value) {
        echo "<p>";
        echo "<b>" . $key . "</b>" . "</p>";
        echo "<p>";
        foreach ($value as $items) {
            echo "<p>";
            foreach ($items as $num=>$item) {
                echo $num . "-";
                echo $item . "<br>";
            }
        }
    }
}


create_file($file_name);
echo "CREATED LINES IN FILE->" . count_lines($dir_file);
echo file_in_page($dir_file);




